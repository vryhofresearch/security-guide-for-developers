[Back to Contents](README.md)


### Appendix A: Links

##### OTHER CHECKLISTS
- [ ] [OWASP Testing Checklist](https://www.owasp.org/index.php/Testing_Checklist)
- [ ] [OWASP Secure Coding Practices Checklist](https://www.owasp.org/index.php/OWASP_Secure_Coding_Practices_Checklist)
- [ ] [OWASP Web Application Security Testing Cheat Sheet](https://www.owasp.org/index.php/Web_Application_Security_Testing_Cheat_Sheet)

##### WEB SECURITY WARGAMES
- [ ] [Natas](http://overthewire.org/wargames/natas/)
- [ ] [Hack this site](https://www.hackthissite.org/)
- [ ] [Hellbound Hackers](https://www.hellboundhackers.org/)